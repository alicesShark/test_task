(function () {
  'use strict';

  if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPad/i))) {
    document
      .querySelector('html')
      .classList
      .add('is-ios');
  }

  var body = $('.body');
  var burger = $('#js-burger');
  var shadow = $('#js-shadow');
  burger.on('click', function () {
    $(this).toggleClass('page-header__burger--active');
    shadow.toggleClass('shadow--showed');
    body.toggleClass('overflow');
  });

  shadow.on('click', function () {
    $(this).removeClass('shadow--showed');
    burger.removeClass('page-header__burger--active');
    body.removeClass('overflow-tablet');
    body.removeClass('overflow');
  });

})();